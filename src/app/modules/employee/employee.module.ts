import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { FormComponent } from './form/form.component';
import { RouterModule } from '@angular/router';
import { EmployeeComponent } from './employee.component';
import { EmployeeRoutes } from './employee.routing';
import { MaterialModule } from 'src/app/material-module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [EmployeeComponent, ListComponent, FormComponent],
    imports: [CommonModule, RouterModule, RouterModule.forChild(EmployeeRoutes), MaterialModule, ReactiveFormsModule],
})
export class EmployeeModule {}
