import { Routes,  } from '@angular/router';
import { EmployeeComponent } from './employee.component';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

export const EmployeeRoutes: Routes = [
    {
        path: '',
        component: EmployeeComponent,
        children: [
            { path: '', component: ListComponent },
            { path: 'add', component: FormComponent },
        ],
    },
];

