import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CountriesService } from 'src/app/services/countries.service';
import { EmployeesService } from 'src/app/services/employees.service';
import { MessagesService } from 'src/app/services/messages.service';
import { Employee } from 'src/app/shared/interfaces/employee';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  loading = false;
  employee!:Employee;
  employeeForm: FormGroup;
  countries: string[] = [];

  get firstName() { return this.employeeForm.get('firstName'); }
  get firstSurname() { return this.employeeForm.get('firstSurname'); }
  get otherNames() { return this.employeeForm.get('otherNames'); }
  get country() { return this.employeeForm.get('country'); }

  constructor(
    private employeeService: EmployeesService,
    private router: Router,
    private countriesService: CountriesService,
    private messages: MessagesService,
    private fb: FormBuilder,
  ) { 
    this.employeeForm = fb.group({
      firstName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(20), Validators.pattern("[a-zA-Z]+")]],
      firstSurname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(20), Validators.pattern("[a-zA-Z]+")]],
      otherNames: ['', [Validators.minLength(2), Validators.maxLength(50), Validators.pattern("[a-zA-Z ]+")]],
      country: ['', Validators.required],
    })
  }

  ngOnInit(): void {
    this.countriesService.getCountries().subscribe(
      res => this.countries = res
    );
    this.employeeForm.get('firstName')?.valueChanges.subscribe(
      val => {
        console.log(val);
        console.log(this.employeeForm.errors);
      }      
    );
  }

  onSubmit() {
    if (this.employeeForm.invalid) {
      return;
    }
    this.getEmployeeFromForm();
    this.employeeService.saveEmployee(this.employee).subscribe({
      next: res => {
        this.messages.showTopCenterMessage('El empleado se creó satisfactoriamente');
        this.router.navigateByUrl('/employees')
      },
      error: err => this.messages.showTopCenterMessage('Error al guardar el empleado', 'snack-error'),
    });
  }

  getEmployeeFromForm(): void {
    this.employee = this.employeeForm.value;
    this.employee.firstName = this.employee.firstName.toUpperCase();
    this.employee.firstSurname = this.employee.firstSurname.toUpperCase();
    this.employee.otherNames = this.employee.otherNames?.toUpperCase();
    this.employee.country = this.employee.country.toUpperCase();
  }
}
