import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { EmployeesService } from 'src/app/services/employees.service';
import { MessagesService } from 'src/app/services/messages.service';
import { AlertComponent } from 'src/app/shared/alert/alert.component';
import { Employee } from 'src/app/shared/interfaces/employee';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css'],
})
export class ListComponent implements OnInit {
    dataSource: Employee[] = [];
    errorMessage?: string;
    loading = false;
    paginator = 0;
    pageSize = 10;
    dataLength = 0;

    constructor(
        private employeesService: EmployeesService,
        public dialog: MatDialog,
        private messagesService: MessagesService
    ) {}

    displayedColumns: string[] = [
        'id',
        'firstSurname',
        'firstName',
        'otherNames',
        'country',
        'email',
        'actions',
    ];

    ngOnInit(): void {
        this.loadDataPage();
    }

    loadDataPage() {
        this.loading = true;
        this.employeesService.getEmployees(this.paginator).subscribe({
            next: (res) => {
                this.dataSource = res?.data ?? [];
                this.dataLength = res?.dataLength ?? 0;
                this.loading = false;
            },
            error: (err) => {
                this.errorMessage = err;
                this.messagesService.showTopCenterMessage(
                    'Error al cargar los Empleados',
                    'snack-error'
                );
                this.loading = false;
            },
        });
    }

    showDeleteDialog(employee: Employee) {
        this.dialog
            .open(AlertComponent, {
                data: {
                    title: 'Eliminar Empleado',
                    msg: `Usted va a eliminar el empleado ${employee.firstName} ${employee.otherNames} ${employee.firstSurname}, ¿Continuar?`,
                    msgButton: 'Sí, eliminar',
                },
                width: '400px',
            })
            .afterClosed()
            .subscribe((res) => {
              if (res) {
                this.employeesService.deleteEmployee(employee.id!).subscribe({
                  next: res => this.loadDataPage(),
                  error: err => this.messagesService.showTopCenterMessage('Ocurrió un error al borrar el empleado, intente nuevamente.', 'snack-error')
                });
              }
            });
    }

    pageChanged(event: PageEvent) {
        this.paginator = event.pageIndex;
        this.loadDataPage();
    }

    deleteEmployee(employee: Employee) {
        this.employeesService.deleteEmployee(employee.id!).subscribe({
            next: (data) => {
                this.messagesService.showTopCenterMessage(
                    'Empleado eliminado satisfactoriamente'
                );
            },
            error: (err) => {
                this.messagesService.showTopCenterMessage(
                    'Error al eliminar el empleado',
                    'snack-error'
                );
            },
        });
    }

    getPaisForHtml(field: string): string {
        let str = '';
        switch (field) {
            case 'COLOMBIA':
                str = 'Colombia';
                break;
            case 'ESTADOSUNIDOS':
                str = 'Estados Unidos';
                break;
            default:
                break;
        }
        return str;
    }
}
