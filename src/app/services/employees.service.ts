import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, of, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Employee } from '../shared/interfaces/employee';

@Injectable({
    providedIn: 'root',
})
export class EmployeesService {
    private baseUrl = environment.baseUrl;
    private employeesUrl = '/employees';
    private url = this.baseUrl + this.employeesUrl;

    constructor(private http: HttpClient) {}

    getEmployees(page: number): Observable<EmployeeResponse> {
        return this.http.get<EmployeeResponse>(`${this.url}?page=${page}`);
    }

    saveEmployee(employee: Employee): Observable<Employee> {
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        if (employee.id) {
            return this.updateEmployee(employee, headers);
        }
        return this.createEmployee(employee, headers);
    }

    deleteEmployee(id: number): Observable<Employee> {
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this.http
            .delete<Employee>(this.url + `/${id}`, {
                headers: headers,
            })
            .pipe(tap((data) => console.log('deleteEmployee: ' + id)));
    }

    private createEmployee(
        employee: Employee,
        headers: HttpHeaders
    ): Observable<Employee> {
        employee.id = undefined;
        return this.http
            .post<Employee>(this.url, employee, {
                headers: headers,
            })
            .pipe(
                tap((data) =>
                    console.log('createEmployee: ' + JSON.stringify(data))
                )
            );
    }

    private updateEmployee(
        employee: Employee,
        headers: HttpHeaders
    ): Observable<Employee> {
        return this.http
            .put<Employee>(this.url, employee, { headers: headers })
            .pipe(tap((data) => console.log('updateEmployee: ' + data.id)));
    }
}

export interface EmployeeResponse {
    data?: Employee[];
    dataLength?: number;
}
