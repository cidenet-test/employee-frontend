import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  constructor(private snackBar: MatSnackBar) { }

  showTopCenterMessage(message: string, strClass?: string, duration?: number) {
    if (strClass != undefined) {
      this.snackBar.open(
        message,
        undefined,
        {
          horizontalPosition: 'center',
          verticalPosition: 'top',
          duration: duration ?? 5000,
          panelClass: [strClass],
        }
      );
    } else {
      this.snackBar.open(
        message,
        undefined,
        {
          horizontalPosition: 'center',
          verticalPosition: 'top',
          duration: duration ?? 5000,
        }
      );
    }
  }
}
