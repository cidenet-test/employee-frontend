import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, tap } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root',
})
export class CountriesService {
    private baseUrl = environment.baseUrl;
    private employeesUrl = '/countries';
    private url = this.baseUrl + this.employeesUrl;
    private countries: string[] = [];

    constructor(private http: HttpClient) {}

    getCountries(): Observable<string[]> {
        if (this.countries.length == 0) {
            return this.reloadCountries();
        }
        return of(this.countries);
    }

    private reloadCountries(): Observable<string[]> {
        return this.http
            .get<string[]>(this.url)
            .pipe(tap((res) => (this.countries = res)));
    }
}
