export interface Employee {
    id?: number;
    firstSurname: string;
    firstName: string;
    otherNames?: string;
    country: string;
    email?: string;
    created?: string;
}
