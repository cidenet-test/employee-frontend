# Employee Frontend

Aplicación web creada en angular 14 para el manejo de empleados, preparada para comunucarse con el API Rest creada en Spring Boot y al cual se puede llegar dando click [aquí](https://gitlab.com/cidenet-test/employee-backend).

El formulario para crear empleados contiene validaciones que aseguran que las restricciones impuestas se cumplan, para que la data enviada al Backend esté sin errores

Para poder correr el proyecto se debe tener instalago el cli de Angular -> [Guía de instalación de Angular](https://angular.io/guide/setup-local)

Este proyecto utiliza la plantilla material-pro-angular-lite que se puede descargar del repositorio en [Github](https://github.com/wrappixel/material-pro-angular-lite)

## Instalación

Se debe clonar este repositorio en el equipo donde se vaya a instalar para continuar con el desarrollo, con la instrucción

```bash
git clone https://gitlab.com/cidenet-test/employee-frontend.git
```

Acceda al a carpeta del proyecto recién clonado

```bash
cd employee-frontend
```

Use el manejador de paquetes [npm](https://www.npmjs.com/) para instalar las dependencias con el comando.

```bash
npm i
```

## Uso

Para ejecutar la aplicación, se debe ejecutar el siguiente comando desde la carpeta principal del proyecto

```bash
ng serve
```

Una vez se termina de levantar el servidor de la aplicación, en el navegador web se debe visitar la dirección [http:localhost:4200](http:localhost:4200) para acceder
